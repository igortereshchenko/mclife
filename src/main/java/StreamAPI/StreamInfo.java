package StreamAPI;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public class StreamInfo {

    public static void main(String[] args){


        ArrayList<String> names = new ArrayList<>();
        names.addAll( Arrays.asList(new String[]{"a","b","c"}  ));


        //count
        System.out.println(names.stream().count());


        //findFirst findAny

        Optional<String> first = names.stream().findFirst();

        System.out.println(first.get());


        Optional<String> any = names.stream().findAny();

        System.out.println(any.get());


        //searching  allMatch   anyMatch  noneMatch  True or False

        boolean all = names.stream().allMatch(name->name.length()==1);

        System.out.println(all);



        boolean any_ = names.stream().anyMatch(name->name=="b");

        System.out.println(any_);


        boolean none = names.stream().noneMatch(name->name.length()>1);

        System.out.println(none);


    }
}
