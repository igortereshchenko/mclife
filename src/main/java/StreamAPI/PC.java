package StreamAPI;

public class PC {

    private String name;
    private  double price;

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public PC(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return "PC{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
