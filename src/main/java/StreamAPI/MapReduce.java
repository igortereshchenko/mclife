package StreamAPI;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class MapReduce {

    public static void main(String[] args){


        Stream<Integer> numbersStream = Stream.of(1,3  ,4,5   ,6,6  ,7,8,9    ,11,32,4,3,2);

        // result = 1    result*=element
        // 1*3     4*5     6*6   7*8*9       11*32*4*3*2
        //  v1   *  v2  *   v3 *   v4    *       v5


        Optional<Integer> result = numbersStream.reduce((x,y)->x*y);

        System.out.println(result.get());





        Stream<String> stringStream = Stream.of("1","3"  ,"4","5"   ,"6","6"  ,"7,8,9"    ,"11,32,4,3,2");


        numbersStream = Stream.of(1,3  ,2,2,3,1,1,3);
        //Optional<String> init = new Optional<>("init value");

        int resultReduce = numbersStream.reduce( 1,
                                                                    (mid_x,mid_y)->{ //mid operator
                                                                        return mid_x*mid_y;
                                                                    },

                                                                    (x,y)->{    // main operator

                                                                        return x + y;
                                                                    }
                                                );

        System.out.println(resultReduce);



//        1,3,2,2,3,1,1,3

//        resultReduce = 1
//        mid 1*3
//        main    resultReduce + mid = 1 +3 = 4


//        resultReduce = 4
//        mid 4*5
//        main    resultReduce + mid = 4 +20 = 24
//...

        /*
        * pc array
        * reduce count total price?
        * */


        PC[] computors = {
                new PC("PC 1", 1099.9),
                new PC("PC 2", 199.9),
                new PC("PC 3", 299.9)
        };



//        PC temp = new PC("",0);
//
//        Optional<PC> resultpc = Arrays.stream(computors).reduce((x,y)->{
//
//            return temp;
////            new PC(x.getName()+" "+y.getName(),x.getPrice()+y.getPrice());
//        });
//
//        System.out.println(resultpc.get());


        double res = Arrays.stream(computors).reduce(0.0,
                                                (x,y)->{
                                                    return x+y.getPrice();
                                                },
                                                (x,y)->
                                                {
                                                    return x+y;
                                                });

        System.out.println(res);
    }
}
