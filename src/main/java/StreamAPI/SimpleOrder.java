package StreamAPI;

public class SimpleOrder {

    private String position;
    private double price;
    private String customer;


    public String getPosition() {
        return position;
    }

    public double getPrice() {
        return price;
    }

    public String getCustomer() {
        return customer;
    }

    public SimpleOrder(String position, double price, String customer) {
        this.position = position;
        this.price = price;
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "SimpleOrder{" +
                "position='" + position + '\'' +
                ", price=" + price +
                ", customer='" + customer + '\'' +
                '}';
    }
}
