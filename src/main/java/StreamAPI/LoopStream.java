package StreamAPI;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Stream;

public class LoopStream {


    public static void main(String[] arg){


        Stream<Integer> numbers = Stream.of(-3,-4,-1,2,3,4,-3,-3,4,-3,0);

        numbers.forEach(value->System.out.println(value));

        //while Java 9
//        numbers.takeWhile(number->number<0).forEach(value->System.out.println(value));


//        numbers.dropWhile(number->number<0).forEach(value->System.out.println(value));


        //numbers.distinct()
        System.out.println("Dublicates removed");

        Stream<Integer> numbers2 = Stream.of(-3,-4,-1,2,3,4,-3,-3,4,-3,0);
        numbers2.distinct().forEach(value->System.out.println(value));




        System.out.println("skip(n)");
        Stream<String> words = Stream.of("w1","w2","w3","w4","w5","w6");

        words.skip(2)
             .forEach(word->System.out.println(word));




        System.out.println("limit(n)");
        Stream<String> words2 = Stream.of("w1","w2","w3","w4","w5","w6");

        words2
                .skip(2)
                .limit(3)
                .forEach(word->System.out.println(word));


        //Pagenation

        String[] data = new String[]{"data1","data2","data3","data4","data5","data6","data7","data8","data9","data10"};


        Scanner scanner = new Scanner(System.in);
        int pageSize = 4;
        int pageNumber = 1;

        while (pageNumber>0){

            System.out.println("Current page is"+String.valueOf(pageNumber));

            Arrays.stream(data)
                    .skip( (pageNumber-1)*pageSize )
                    .limit(pageSize)
                    .forEach(value->System.out.println(value));


            System.out.println("Enter page number");
            pageNumber = scanner.nextInt();




        }


/*
* 1. array PC
* 2.
*   group1  - PC1 PC2 PC3
*   group2  - PC4 PC5 PC6
*   group3  - PC7
*
* 3.Inside group order by price
*
*   group1  - PC2 PC3 PC1
*   group2  - PC4 PC6 PC5
*   group3  - PC7
*
* */










    }
}
