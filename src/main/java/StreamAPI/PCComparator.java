package StreamAPI;

import java.util.Comparator;

public class PCComparator implements Comparator<PC> {
    @Override
    public int compare(PC pc1, PC pc2) {

        return (int) Math.round(pc1.getPrice()-pc2.getPrice());
    }
}
