package StreamAPI;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Runner {


    public static int threadResult=0;



    static public void main(String[] arg) {

        int[] numbers = {-5, 2, -1, 2, 3, 4, -11, -1, 0, 23, 12};

        int count = 0;

        for (int i : numbers
        ) {
            if (i > 0) count++;
        }

        System.out.println(count);


        //TODO count negative by two thread
        /*
            divide array / 2 part
            first part => 1 thread
            second part => 2 thread


            result = thread1.result + thread2.result
        */


        //TODO rewrite using Runnable interface
//        int[] firstPart = {1,2-3,4};
//
//        new Thread(()->{
//
//            int firstPartCount = 0;
//
//            for (int i: firstPart
//            ) {
//                if (i>0) firstPartCount++;
//            }
//
//            synchronized (Runner.threadResult){
//                Runner.threadResult+=firstPartCount;
//            }
//
//        }).start();
//
//
//
//
//
//        int[] secondPart = {1,2-3,4};
//
//        new Thread(()->{
//
//            int secondPartCount = 0;
//
//            for (int i: secondPart
//            ) {
//                if (i>0) secondPartCount++;
//            }
//
//
//            synchronized (Runner.threadResult){
//                Runner.threadResult+=secondPartCount;
//            }
//
//        }).start();
//
//
//        System.out.println(threadResult);


        IntPredicate predicate = new IntPredicate() {
            @Override
            public boolean test(int value) {
                return value > 0;
            }
        };

        long result = IntStream.of(-5, 2, -1, 2, 3, 4, -11, -1, 0, 23, 12).filter(predicate).count();


        long resultLambda = IntStream.of(-5, 2, -1, 2, 3, 4, -11, -1, 0, 23, 12).filter(value -> value > 0).count();


        System.out.println(result);
        System.out.println(resultLambda);


        ArrayList<String> cities = new ArrayList<>();

        cities.add("C1");
        cities.add("C2");
        cities.add("C3");


        Collections.addAll(cities, "C4", "C5", "Abc", "C6");


        for (String city : cities
        ) {
            if (city.length() == 3)
                System.out.println(city);
        }


        cities.stream().filter(city -> city.length() == 3).forEach(city -> {

            System.out.println(city);
//            ....
        });

//way1
        Stream<String> cityStream = Stream.of("a", "b");


//way2
        String[] data = {"a", "b", "c"};
        Stream<String> cityStream2 = Stream.of(data);


//way3
        Stream<Double> cityStream3 = (Stream<Double>) Arrays.stream(new Double[]{1.2, 1.2});


        PC[] pcData = {new PC("PC 1", 1099.9), new PC("PC 2", 299.9), new PC("PC 3", 399.9)};


        Stream<PC> pcStream = Stream.of(pcData);


        pcStream
                .filter(pc ->
                        pc.getPrice() > 200.0
                )
                .forEach(
                        pc ->
                                System.out.println(pc.getName())
                );


        //Map all PC to SimpleOrder

        List<PC> pc = new ArrayList<>();
        pc.add(new PC("PC 1", 1099.9));
        pc.add(new PC("PC 2", 199.9));
        pc.add(new PC("PC 3", 299.9));


        List<SimpleOrder> orders = new ArrayList<>();

        for (PC pc_ : pc) {
            orders.add(new SimpleOrder(pc_.getName(), pc_.getPrice(), "Some customer"));
        }


//
//        Stream<String> simpleOrderStream = pcStream.map(computor->{
//
//            return computor.getName();
//        });
//
//        // wait
//
//        simpleOrderStream.forEach(simpleOrder -> System.out.println(simpleOrder));

        pcStream = Stream.of(pcData);

        // map to string
        pcStream.map(computor -> {

                        return computor.getName();
                     })
                .forEach(s -> System.out.println(s));





        pcStream = Stream.of(pcData);

        // map to SimpleOrder
        pcStream.map(computor -> {

            return new SimpleOrder(computor.getName(), computor.getPrice(), "Some customer");
        })
                .forEach(s -> System.out.println(s));




//        public class Phone {
//
//            // model
//            // number
//            // Human
//        }
//
//        //TODO MAP List<Phone> -> SimpleOrders -> print


        //comparator
        pcStream =  Stream.of(pcData);

        pcStream
                .sorted(new PCComparator())
                .forEach(computor->System.out.println(computor));


    }
}
