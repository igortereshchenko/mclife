package LessonThread.McLife;

import java.util.List;
import java.util.concurrent.Phaser;

public class Cooker implements ICooker {

    @Override
    public int getProductivityCount() {
        return productivityCount;
    }



    private Kitchen kitchen;

    private int productivityCount;

    private Phaser phaser;


    public Cooker(int productivityCount, Kitchen kitchen) {
        this.productivityCount = productivityCount;
        this.kitchen = kitchen;

        this.phaser = kitchen.getPhaser();
        phaser.register();
    }



    @Override
    public void run() {

        Object lock = kitchen.getLock();


        while(!kitchen.isWorkDayFinished()) {

            //phaser.register();

            int countToCook = kitchen.getCountBurgersToCook(this);

            if (countToCook==0) {
//                try {
//                    System.out.printf("Cooker: productivity = %d is waiting \n",productivityCount);
//
//                    synchronized (lock) {
//                        lock.wait();
//                    /*
//                    HOME WORK wait until all finished
//                    Notify:
//                        new order
//                        work day finished
//                        ...
//                    */
//                    }
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }

                System.out.printf("Cooker: productivity = %d is waiting \n",productivityCount);
                phaser.arriveAndAwaitAdvance();

            }
            else {


                for (int i = 0; i < countToCook; i++) {

                    kitchen.burgerDone(new Burger());


                    try {
                        Thread.sleep(500);

                        System.out.printf("Cooker: productivity = %d has cooked %d burgers \nCount of burgers %d \n", productivityCount, i + 1, kitchen.getBurgerCount());


                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }


//            synchronized (lock) {
//                lock.notifyAll();
//            }

//                phaser.arriveAndAwaitAdvance();
            }
        }


    }


}
