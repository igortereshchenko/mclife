package LessonThread.McLife;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Kitchen {



    private static final class LockForCooker{};

    private final Object lock = new LockForCooker();


    public synchronized Object getLock() {
        return lock;
    }


    //----------------------------------------

    private List<ICooker> staff;       // all staff

    private List<IBurger> burgers;     // all cooked burgers on Kitchen


    private int orderedCountOfBurgers; // all orders


    private int progressCountOfBurgers;// all progressed


    private Phaser phaser;



    public synchronized Phaser getPhaser(){
        return phaser;
    }

    //----------------------------------------


    public synchronized List<IBurger> getBurgers() {
        return burgers;

    }

    //---------------------------------------- Staff

    public synchronized int getOrderedCountOfBurgers() {
        return orderedCountOfBurgers;
    }

    public synchronized void setOrderedCountOfBurgers(int orderedCountOfBurgers) {
        this.orderedCountOfBurgers = orderedCountOfBurgers;
    }



    //---------------------------------------- Cooker


    public synchronized int getProgressCountOfBurgers() {
        return progressCountOfBurgers;
    }

    public synchronized void incProgressCountOfBurgers(int addCountOfBurgers) {
        this.progressCountOfBurgers += addCountOfBurgers;
    }

    //burgers done
    public synchronized void burgerDone(IBurger burger) {
        this.progressCountOfBurgers -= 1;
        this.burgers.add(burger);

    }

    public synchronized int getBurgerCount(){

        burgerCount = burgers.size();
        return burgerCount;
    }


    public synchronized  int getCountBurgersToCook(ICooker cooker){

        int count = Math.max(orderedCountOfBurgers - burgers.size() - progressCountOfBurgers,0);

        int countToCook = Math.min(count,cooker.getProductivityCount());

        incProgressCountOfBurgers(countToCook);

        return countToCook;
    }



    public synchronized boolean isWorkDayFinished(){

        return orderedCountOfBurgers==0;
    }




    private int burgerCount;


    public Kitchen(int staffCount) {

        phaser = new Phaser(1);

        staff = new ArrayList<>();
        burgers = new ArrayList<>();


        for (int i = 0; i < staffCount; i++) {
            staff.add(new Cooker(i+1,this));
        }

    }






//TODO List<Orders>
    
    public void cookBurger(int count){




        //Stage 1

        this.orderedCountOfBurgers = count;

        for (ICooker cooker : staff) {
            new Thread(cooker).start();
        }

        // Stage 2


        phaser.getPhase();
        phaser.arriveAndAwaitAdvance();

//while(is not all order ready)
//        synchronized (lock) {
//            lock.wait;
//        }



        //HOME WORK wait until all finished
        System.out.printf("Done Count of burgers %d\n\n",getBurgerCount());


        for (int i = 0; i < count; i++) {
            burgers.remove(0);
            orderedCountOfBurgers--;
            System.out.println("Burger has been given to client");
        }




        // Stage 3

        //Notify all order is finished!!!

        phaser.getPhase();
        phaser.arriveAndDeregister();

//        synchronized (lock) {
//            lock.notifyAll();
//        }
    }



}
